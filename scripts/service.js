import axios from '~/plugins/axios'
import * as Utils from '~/scripts/utils.js'

export const getUUID = async url => {
  const { data } = await axios
    .get(`${process.env.colomboUrl}scan/${url}`)
    .catch(console.log('Error: Cannot GET website identifier'))
  return data
}

export const getSummary = async uuid => {
  const { data } = await axios
    .get(`${process.env.colomboUrl}response/${uuid}`)
    .catch(console.log('Error: Cannot GET sumary report'))
  return data
}

export const getBoxStatus = async uuid => {
  const boxes = [
    'web_optimization',
    'search_optimization',
    'rrss_optimization',
    'seo_optimization',
    'ppc_optimization'
  ]
  const promises = []
  const formattedResp = {}
  boxes.forEach(el => {
    promises.push(
      axios
        .get(`${process.env.colomboUrl}request/${uuid}/box/${el}`)
        .catch(console.log('Error: Cannot GET box status'))
    )
  })
  const response = await Promise.all(promises)
  response.forEach((el, index) => {
    formattedResp[boxes[index]] = el.data.result.status
  })
  return formattedResp
}

export const setBoxStatus = async (uuid, boxes) => {
  const promises = []
  boxes.forEach(el => {
    promises.push(
      axios
        .post(
          `${process.env.colomboUrl}request/${uuid}/box/${el.boxName}/
${el.status}`
        )
        .catch(`Error: Cannot POST attribute ${el.boxName}`)
    )
  })
  const response = await Promise.all(promises)
  if (response.some(el => el.status !== 200)) {
    console.error('Failed to POST box status')
    return false
  } else {
    return true
  }
}

export const getFormFields = async uuid => {
  const fields = [
    'catalog',
    'description',
    'picture',
    'blog',
    'video',
    'competitor_url1',
    'competitor_url2',
    'competitor_url3',
    'business',
    'address',
    'postalcode',
    'telephone',
    'city',
    'epigraph',
    'keywords',
    'market',
    'activity',
    'google_ads',
    'google_calls',
    'facebook_ads'
  ]
  const { data } = await axios
    .get(`${process.env.colomboUrl}request/${uuid}/fields/?keys=${fields}`)
    .catch(console.log('Error: Cannot GET form fields'))
  return data.result
}

export const setFormFields = async (uuid, fields) => {
  const { data } = await axios
    .get(`${process.env.colomboUrl}request/${uuid}/fields/?${fields}`)
    .catch(console.log('Error: Cannot GET form fields'))
  const { status } = data
  if (status === 'OK') {
    return true
  } else {
    return false
  }
}

export const getEpigraphSuggestions = async query => {
  const { data } = await axios
    .get(
      `http://cms.xyz.com/api/v1/directory-categories/?detailed=1&name__icontains=${query}`
    )
    .catch(console.log('Error: Cannot GET epigraph suggestions'))
  const epigraph = data.results.map(
    epi => (epi = { value: epi.name, refNumber: epi.id })
  )
  return epigraph
}

export const getLocationSuggestions = async query => {
  const { data } = await axios
    .get(`http://cms.xyz.com/seo/locations/get/?filter=${query}`)
    .catch(console.log('Error: Cannot GET location suggestions'))
  const locations = data.map(
    loc => (loc = { value: loc.text, refNumber: loc.id })
  )
  return locations
}

export const getSeoMonkey = async (category, location) => {
  const { data } = await axios
    .get(
      `http://cms.xyz.com/seo/expressions/get/?category=${
        category.ref
      }&location=${location.ref}`
    )
    .catch(console.log('Error: Cannot GET seo monkeys'))
  const { available } = data
  const seoExp = []
  available.forEach(el => {
    seoExp.push(el.replace(location.value, '').trim())
  })
  const seoKeys = await axios({
    method: 'post',
    url: `https://dashboard.optimizaclick.com/api/v2/keyword-validator/local/list/xyz/seo/${
      location.value
    }`,
    headers: {},
    data: {
      keywords: seoExp
    }
  })
  const { keywords } = seoKeys.data.result
  Object.keys(keywords).forEach(el => {
    if (
      !(
        keywords[el].response &&
        keywords[el].response.validator &&
        keywords[el].response.validator.is_valid === 'valid'
      )
    ) {
      delete keywords[el]
    }
  })
  if (Utils.isOdd(Object.keys(keywords).length)) {
    const lowestROI = Object.values(keywords).find(el =>
      Math.min(el.response.parameters.roi)
    )
    delete keywords[lowestROI.response.parameters.keyword]
  }
  return {
    keywords,
    total: Object.keys(keywords).length
  }
}

export const setSeoMonkey = async (uuid, seoList, seoLength) => {
  // reduce to set number of keywords
  while (seoLength < Object.keys(seoList).length) {
    const lowestROI = Object.values(seoList).find(el =>
      Math.min(el.response.parameters.roi)
    )
    delete seoList[lowestROI.response.parameters.keyword]
  }
  const { data } = await axios({
    method: 'post',
    url: `${process.env.colomboUrl}sources/${uuid}`,
    headers: {},
    data: {
      keywords: seoList
    }
  })
  const { status } = data
  return status === 'OK'
}
