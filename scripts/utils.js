const formatNum = num => num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')

const splitDate = dateString => {
  // - dateString format yyyy/mm/dd
  const date = dateString.split('-')
  return {
    year: parseInt(date[0], 10),
    month: parseInt(date[1], 10),
    day: parseInt(date[2], 10)
  }
}

const objToArr = obj => {
  const toArray = Object.keys(obj).map(key => {
    return obj[key]
  })
  return toArray
}

const hexToRGB = (hex, alpha) => {
  const r = parseInt(hex.slice(1, 3), 16),
    g = parseInt(hex.slice(3, 5), 16),
    b = parseInt(hex.slice(5, 7), 16)
  if (alpha) {
    return `rgba(${r}, ${g}, ${b}, ${alpha})`
  } else {
    return `rgb(${r}, ${g}, ${b})`
  }
}

const isOdd = num => num % 2

export { formatNum, splitDate, objToArr, hexToRGB, isOdd }
